---
title: ${1:$TM_FILENAME_BASE}
description: ${2:Description}
foam_template:
  name: Audio post
  description: What music did you make today?.
  filepath: site/notes/$FOAM_TITLE.md'
tags: audio
---

${4:Content!}
