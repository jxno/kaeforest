---
title: ${1:$TM_FILENAME_BASE}
description: ${2:Description}
foam_template:
  name: Daily Note
  description: A note on your daily activies and goals.
  filepath: 'site/notes/$FOAM_TITLE.md'
tags: daily-note
---

${4:Content!}

## Objectives

${5:Objectives}

## Todo

${6:Todo}
