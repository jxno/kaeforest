document.addEventListener("DOMContentLoaded", () => {
  // Custom cursor
  const cursor = document.getElementById("custom-cursor")
  const cursorElements = document.querySelectorAll("[data-cursor-text]")

  if (window.innerWidth >= 768) {
    document.addEventListener("mousemove", (e) => {
      cursor.style.left = `${e.clientX}px`
      cursor.style.top = `${e.clientY}px`
    })

    cursorElements.forEach((element) => {
      element.addEventListener("mouseenter", () => {
        cursor.textContent = element.getAttribute("data-cursor-text")
        cursor.classList.add("text")
      })

      element.addEventListener("mouseleave", () => {
        cursor.textContent = ""
        cursor.classList.remove("text")
      })
    })
  }

  // Menu toggle
  const menuButton = document.getElementById("menu-button")
  const menuIcon = document.getElementById("menu-icon")
  const fullscreenMenu = document.getElementById("fullscreen-menu")

  menuButton.addEventListener("click", () => {
    fullscreenMenu.classList.toggle("open")
    menuIcon.textContent = fullscreenMenu.classList.contains("open") ? "✕" : "☰"
  })

  // Back to top
  const backToTopButton = document.getElementById("back-to-top")

  backToTopButton.addEventListener("click", () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    })
  })

  // Animations
  const animateFadeUp = document.querySelector(".animate-fade-up")
  const animateFadeIn = document.querySelector(".animate-fade-in")
  const animateOnScroll = document.querySelectorAll(".animate-on-scroll")
  const animateSlideIn = document.querySelectorAll(".animate-slide-in")

  // Initial animations
  setTimeout(() => {
    if (animateFadeUp) animateFadeUp.classList.add("is-visible")
  }, 200)

  setTimeout(() => {
    if (animateFadeIn) animateFadeIn.classList.add("is-visible")
  }, 400)

  // Scroll animations
  const observerOptions = {
    threshold: 0.1,
    rootMargin: "0px 0px -100px 0px",
  }

  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        entry.target.classList.add("is-visible")
        observer.unobserve(entry.target)
      }
    })
  }, observerOptions)

  animateOnScroll.forEach((element) => {
    observer.observe(element)
  })

  // Social links animation
  animateSlideIn.forEach((element, index) => {
    setTimeout(
      () => {
        element.classList.add("is-visible")
      },
      300 + index * 100,
    )
  })
})


