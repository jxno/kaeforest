---
title: Me
description: A specter in the wired.
tags: ['nav', 'contact']
order: 4
image: "/static/content/img/self/IMG_7789.jpg"
---

## Socials

{% for link in links %}
<a href={{link.url}}>{{link.user}} on {{link.name}}</a>
{% endfor %}


I often check my emails, so I recommend just shooting me one if you like-
You can just grab my public key with this command, or download it [here](/static/content/key.asc) if you don't feel like opening a terminal.

`curl {{site.url}}/static/content/key.asc`

And our address is ~ <a href="mailto:{{site.author.email}}">here!</a> - m (at) kae (dot) si



