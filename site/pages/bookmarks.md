---
title: Bookmarks
description: The bookmarks directory.
image: /static/img/blooming-flowers.gif
layout: layouts/home.njk
tags: nav
order: 5
---

## Music

### M01 27 2025

- [Uprising - Commodo :: 
Classic UK Dub. Literally takes me back to my days in long drives through the Blackwall tunnel to go to loads of raves around Essex....](https://open.spotify.com/track/1gc2gHJUgYnp5b5mB8NEKO?si=7cf66e0befcf4195)



- [Little Fluffy Clouds - The Orb :: 
Some of the nicest electronic I've heard in a while, in my quest to find soul in music lately this definately fills the gap for me :)](https://open.spotify.com/track/0SiXUBgqtFRk8rGKd0z3LH?si=9bb79a40745641a2)


- [Black Messiah - D'Angelo ::
A telltale album for black revolution. Honestly this album really called to me in the struggles of my people being put into very cool punk-ish neo-soul for the marginalised.
Highly recommend listening if you're a fan of extremely well-written grunge music, with a much harder hitting message.](https://open.spotify.com/album/5Hfbag0SsHxafx1SySFSX6?si=-nGNmxXkSeqgsjDUec0GuA)


### M04 04 2024
- [Miniscopie, by Reine](<https://beldamrecords.bandcamp.com/album/miniscopie>)

- [Telekinetic, by Aliceffekt](<https://aliceffekt.bandcamp.com/album/telekinetic>)

## Reading List

### M01 27 2025

- [A deep dive into LLMs and their perceived "hallucinations". Worth a read if you're remotely interested in AI tech.](https://dotkay.github.io/)

- [Interesting thought experiment into the sentience of trees, based off of old Sikhism belief and tale](https://e360.yale.edu/features/are_trees_sentient_peter_wohlleben)

- [Places to explore in London](https://www.28dayslater.co.uk/threads/researching-places-to-explore-in-london.130710/)

### M08 23 2023

- [Vagus Nerve Stimulation - Gerhard Rogler](<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5859128/>)

- [Hamonshū by Mori, Yūzan, - 1917](<https://archive.org/details/hamonshuyv3mori>)

- [CCRU - Zone 3](<http://ccru.net/zones/Zn3.htm>)

- [Project Cyberpunk](<http://project.cyberpunk.ru/idb/books.html>)

- [Florian Hecker - Resynthesisers](<https://www.urbanomic.com/book/hecker-resynthesizers/>)

- [A zen Guid to Paper & Pen Games](<https://zenseeker.net/BoardGames/PaperPenGames.htm>)

- [Ghosts in the Shell - Lainzine](<https://haidishe.neocities.org/posts/zine5/ghosts_in_the_shell/>)

- [Xenogothic](<https://xenogothic.com/2021/03/14/the-spectre-of-acid-communism/>)
