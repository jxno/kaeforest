---
title: Friends
description: Signal-boosting friends' sites you should check out
layout: layouts/home.njk
---

##  These are people I have either known for years, or are acquainted with.

If you're not here, I can add you! DM me. :eye:
Sadly not all my friends have websites.

{% for friend in friends %}
<a href={{friend.url}}>{{friend.name}}</a>
{% endfor %}
