---
title: Japanese
description: |
  Notes on the Japanese language.
date: 2022-10-05
tags:
  - language
---

![Tokyo Cityscape](/static/img/barbican-tower.jpg)

The Japanese language is a phonetic language written in Hiragana. To understand Hiragana, I've prepared a smol introduction for you:


Hiragana is a complex, symbol-based syllable writing form used for the Japanese language derived from *kana*. Each *mora* (similar to a syllable based on phonetics) is represented by a singular character *or digraph*.

There are 5 vowels, being:

| English | Hiragana |
| :---    |     ---: |
|    a    |    あ    |
|    i    |    い    |
|    u    |    う    |
|    e    |    え    |
|    o    |    お    |

And 42 consonant-vowel unions:

- へ Being pronounced é when used as a particle
- を Being used as a particle & in *some* names. It is often pronounced *o* instead.
- ん Used as a singular consonant, used for *n* and *mu*.

While other systems usually use a character known as *chōonpu*, aka. the long vowel mark, it's rarely used in Hiragana, unless when written in the Okinawan dialect, and in the word らーめん, *rumen*. But this is seen as non-standard, mainly since most Tokyo residents look down on Okinawans.

/jk

We heavily recommend taking a look at the [hiragana chart](<https://www.learn-japanese-adventure.com/hiragana-chart.html>) to gain a full overview of the available character set within Hiragana, and how to form & construct words & sentences.

