---
title: Titans
description: Titans were designed to manipulate and study the Void.
date: 2023-05-22
---

The Titans, developed by the [[CFS]] were wrought into existence in a vain attempt to study the [[Void]] as a means of harvesting their capabilities to create a form of recursive energy source.