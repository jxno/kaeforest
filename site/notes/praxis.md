---
title: Crafting Tree
description: The main progression to unlock items in Playtime.
tags: [playtime, dev, art]
date: 2024-09-13
---

A pacifist player may only be able to unlock modes of peace and egalitarianism through practicing this mode of particular praxis - á la, hiding from the hoardes of [[vessels]], rather than outright fighting or killing them.

Or a puritan player may wish for the opposite - to aim for the extermination of all remaining [[vessels]] and becoming a memetic opposing force to the [[CFS]], which may have negative karmatic consequences.

