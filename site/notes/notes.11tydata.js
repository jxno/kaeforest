// import titleCase from 'title-case'

// This regex finds all wikilinks in a string
const wikilinkRegExp = /\[\[\s?([^[\]|\n\r]+)(\|[^[\]|\n\r]+)?\s?]]/g;

const caselessCompare = (a, b) => a.toLowerCase() === b.toLowerCase();

export default {
	layout: 'layouts/wiki.njk',
	tags: ['notes'],
	type: 'note',
	eleventyComputed: {
		title: data => (data.title || data.page.fileSlug),
		backlinks: async (data) => {
			const notes = data.collections.notes;
			const currentFileSlug = data.page.fileSlug;

			let backlinks = [];

			// Search the other notes for backlinks
			for (const otherNote of notes) {
				const frontMatter = await otherNote.template.read();

				// Get all links from otherNote
				const outboundLinks = (frontMatter.content.match(wikilinkRegExp) || [])
					.map(link => (
						// Extract link location
						link.slice(2, -2)
							.split("|")[0]
							.replace(/.(md|markdown)\s?$/i, "")
							.trim()
					));

				// If the other note links here, return related info
				if (outboundLinks.some(link => caselessCompare(link, currentFileSlug))) {

					// Construct preview for hovercards
					let preview = frontMatter.content.slice(0, 240);

					backlinks.push({
						url: otherNote.url,
						title: otherNote.data.title,
						preview
					})
				}
			}

			return backlinks;
		}
	}
}