---
title: Boat
description: A means of escape, and discovery.
tags: [playtime]
date: 2024-09-13
---

<img
	src="/static/content/img/boat.jpg"
	alt="boat"
	width="580px"
/>

The boat in [[playtime]] is the starting goal for any new player.
Through the boat, the player can invite other players into their world to visit, and venture into the vast expanses of the rest of the world, in the chance they may counter a new world to explore, a player to meet, or an ancient ruin to discover lore.
