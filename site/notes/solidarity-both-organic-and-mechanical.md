---
title: Organic and Mechanical solidarity
description: The bi-partisan relationship of human homogeneity and kinship.
date: 2023-11-28
tags: sociology
---

The terms describe a system of shared human understanding, in which a collective forms a collective conscious through either shared labour, age, gender and lifestyle, or through the kinship of familiar and familial networks[^1].

## Organic Solidarity

An organic solidarity is formed under the pretenses of a shared workload sustained by a community to meet the needs of the larger organism.
For this reason, the labour required for the upkeep of such an organism is more greatly shared by each subsequent organelle, much like the growth and expansion of a living body[^2].
This is in stark contrast to a group of living bodies that live under an imposed form of law and governance, which would be unsuitable for such an environment; think back to [[Deleuze]]'s teachings on the body without organs - to sever strata in excess would be in-conducive to the growth of of a healthy organism, and through the restriction of ideas and desire, the organism effectively dies.

The strength of an organic solidarity stems from its freedom of growth and mutual shared understanding, but its downfalls arise from its susceptibility to a foreign external power, or memetic poisoning.

## Mechanical Solidarity

A mechanical solidarity is social integration of members through mutual values and beliefs. These constitute what Durkheim calls the ++[[collective-unconscious]]++, that works internally within the individual organelles, forming bonds, leading to co-operation of each of the strata[^2].
In Durkheim's view, the forces that cause the members of society, the organelles, to co-operate are adjacent in theory to how molecules in the realm of physics operate - where covalent bonds must join together in order to survive and form a solid.
This is the most liberating and artistic, and often cited "correct" view of the [[collective-unconscious]] and its operation, where each strata is free to grow in its own unique sense, promoting the healthy growth of the organism.

[^1]: [Wikipedia - Mechanical and Organic Solidarity](<https://en.wikipedia.org/wiki/Mechanical_and_organic_solidarity>)
[^2]: [Britannica - Mechanical & Organic Solidarity](<https://www.britannica.com/topic/mechanical-and-organic-solidarity>)
