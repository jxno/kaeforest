---
title: Playtime
description: An immserion simulator to create and share your world.
tags: [playtime]
date: 2024-09-13
layout: layouts/post.njk
image: "/static/content/img/globe_vision.png"
---

Playtime is a game which spawns you on a distant island, with no connection to any other player.
From here, the player is forced to create and immerse themselves in a world of their own creation, building a starter home for survival against the cold and hostile nights that lay ahead.

## Modding Playtime :computer:

Read [here](/notes/developing_playtime) for all notes on how to work with the playtime source code.

## Survival :knife:

The nights the player must endure will be long, tiring and unfruitful. Only through a constant state of strife - the climate of the island, whether it be by its own natural state or through the player's alteration, the hoardes of [[vessels]] that come in the night to restore the island to its natural order, natural disasters that occur from the time of year the player may spawn in - only through ++all++ these trials may they be finally be able to see their world come to light, in the hopes that they may one day be able to craft a [[boat]] and leave the island, for good.

But to do this, they must first learn the essence of survival, crafting and leveling their own self - their traits, and how they perceive the world, their items - how they interact with it, and their praxis - their resolve in which the player will abide by in order to find how they wish to *live*.

## Weapons, Skills and Customisation

New [[weapons]], structures and other game objects to aid the player can be unlocked through the [[crafting_tree]].
The choices in the player's playstyle will largely determine the outcome of what is unlockable within this tech tree.

## Currency // Battery Packs

Currency comes in the form of [[battery_packs]]. Each batteries' value holds a max of **30** units.
These batteries can then be used & traded to purchase higher level items, and trade with players.

## Suit functionality

The suit in the game is automatically added to the player. It is charged to **100** by default.
The max it can be overcharged to is **150**

## Extra features

Below is a list of features implemented in the game.

- The deathcam has been added to the game, you can now see how you last died & when, call of duty style.
