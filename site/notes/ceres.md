---
title: Ceres
description: Ceres is an automated IoT vertical farming app designed to study the effects of music on plants.
date: 2023-12-18
tags:
- audio
- dev
---

Dating back to an idea spawned in late 2022, Ceres is built to study the growth of plants affected by music, with their exterior environment being heavily regulated and monitored by a system of custom built IoT devices.

## Working Theory

Ceres aims to stimulate plant growth through the use of chaotically generated sound and other genres of music.
The methods in which I talk about this are specified in my writings about [[synthesis]] and the [[ueda-attractor]].

[Ceres on GitLab](https://gitlab.com/kaelta/kwn)
