---
title: Ueda Attractor
description: The Ueda Attractor is a chaotic mathemetical model used in engineering.
date: 2024-01-07
tags: audio
---

In short, the Ueda Attractor is a [phase solution](<https://tutorial.math.lamar.edu/classes/de/phaseplane.aspx>) based on the [Duffing Equation](<https://en.wikipedia.org/wiki/Duffing_equation>), using the formula:
$$
x^..+delta x^.+(beta x^3+/-omega 0^2x)=Ɣcos(Ωt+φ)
$$

It was a concept in Electronic Engineering created by a Japanese researched called Yoshisuke Ueda.
I've linked some of his works [here](/static/content/Chaotic%20Sound%20Synthesis%20-%201998.pdf)

[^1]: <https://www.flyingcoloursmaths.co.uk/dictionary-of-mathematical-eponymy-ueda-attractor/>
