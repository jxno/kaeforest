---
title: Audio
description: Selected audio works & production.
date: 2024-09-08
tags: pages
---

<div class="grid" style="display: grid; grid-template-columns: 2;">

  <section style="max-width: 420px;">

  ## The Drake Equation - 2024-12-2025
  <div class="flex gap-4 m-auto">
  <p>Links:</p>
  <a href="https://soundcloud.com/d33rgrrlzz/sets/the-drake-equation/s-GFvHvkmbGUG?si=dcb9ebdb6e444817b00fb754679f64c6&utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing">
    Soundcloud
  </a>
  </div>

  <img
    src="/static/content/img/enterthevoid.jpg"
    alt="The Drake Equation Cover Art"
    width="320"
  />

  A demo that will never be finished, after losing my macbook this year.

  *Time: 21:38*
  *Track Length: 4*
  *Produced: Kae Machi*
  </section>

  <section style="">

  ## Receiving Transmissions, 12-10-2024

  <div class="flex gap-4 m-auto">
  <p>Links:</p>
  <a href="https://soundcloud.com/d33rgrrlzz/receiving-transmissions">Soundcloud</a>
  <a href="https://d33r.bandcamp.com/receiving-transmissions">Bandcamp</a>
  </div>


  <img
    src="/static/content/img/rec-transmissions.cover.png"
    alt="Receiving Transmissions Cover Art"
    width="320"
  />


  My debut album, produced in Sheffield.

  *Time : 42:18*
  *Track Length : 14*
  *Produced : Kae Machi*
  *Mixed & Mastered : Paraxis (Llydia Cross)*
  *Cover art : xhaart*
  </section>
</div>

------

## Notes and writeups

{% for post in collections.audio %}
  <a href="{{post.url}}"> {{post.data.title}} </a> - <o> {{post.data.description}} </o>
{% endfor %}
