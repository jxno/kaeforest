---
title: Amsterdam
description: Photos from our brief time in Amsterdam
date: 2023-11-29
tags: travel
---

<style>
img,
p>img {
	max-width: 70vw;

	@media (max-width: 450px) {
		max-width: 90vw;
	}
}
</style>

![Amsterdam Canals](/static/img/amsterdam_canals.jpg)
<cite>Waterways | 201A2</cite>

![Streets](/static/content/img/amsterdam.jpg)
<cite>Streets | 2f9A2</cite>

![view](/static/content/img/amsterdam-bar-view.jpeg)
<cite>I liked the street look a lot for some reason. | 393f2</cite>


