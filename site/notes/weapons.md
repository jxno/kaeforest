---
title: Weapons of Playtime
description: The weapon system
tags: [playtime]
date: 2024-09-13
---

Weapons in [[playtime]] scale in effectiveness with the level of the player.
A stick for example, in the start of the game, will not be able to keep up with the player's demands, as the stick only has a certain cutoff for its capabilities, requiring the player to craft a strong weapon, or to reinforce their current one - á la, an iron-clad stick.

