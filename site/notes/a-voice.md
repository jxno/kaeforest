---
title: A poem for someone
description: .
date: 2024-12-25
tags: writeup
---

![lost](/static/content/img/barbican/text.jpeg)

Your voice comes from your heart. With every word you speak, you project it. Your love goes into every word you speak, every action you take.
Listen to it, after all, it loves you and everyone you love.

Your love comes from your inner child, and yes, you do still have one :)

The world is not built on hate, but love. You can feel the love in the universe, from the people you met, the sounds of the birdsong, the bricks upon which you step, the gentle caress of the wind. You are the love, and the love is you. The love is the universe. And the universe loves you.

There are terrible things in this world, which you've seen, felt, heard. Horrible things that no one should suffer, but do regardless. One of these souls may be you, a loved one, or someone else far away, next to you - a stranger even. Treat them with love, they need it just as much as you.

Whenever you may wander, bring a light where darkness seeps. Let your flame burn bright as Polaris, and where you can, leave a trail so others may follow the path. 
When there is no guide, become one. When there is no mother, become one. The fire inside you yearns to manifest its desires, so let it.

You deserve it.
