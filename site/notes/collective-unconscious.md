---
title: Collective Unconscious
date: 2023-11-07
description: The collective unconscious is the subliminal and subconscious connection of human souls.
tags: ['writeup', 'sociology', 'postmodernism']
---

The theory of collective-unconsciousness was first coined by [[durkheim]] in *Suicide*, *The Division of Labour in Society* and *Elementary Forms of Religious Life*.
Durkheim argued that "traditional", or primitive societies (that being, one based on tribes, clans etc.) or even a totemic religion, played a pivotal part in uniting members through a common consciousness, united through common shared praxis, maxims and values.
This is believed to grow a marked fondness for fellow man.

This spawns a network of like minded people, which develop a remarkable underlying sense for one another's presence, emotions, and desires.
It would stand to reason that this is the underlying reason as to the true nature of human connection and society - this, drive to further one another and grow to strength the connection between one another; it develops a sort of, *culture sans frontières*
