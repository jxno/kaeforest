---
title: Developing Playtime
description: Notes on how devving on playtime works
tags: [playtime]
layout: layouts/post.njk
date: 2024-09-13
---

## Foreword

The codebase does not use *any* C++ default namespaces by standard - this was introduced in C++ 1995, which was before the time where HL1 had started development.
The only C++ *feature* used within the code is the `class` type, but everything in the game is registered to the singular global namespace.
This is done to improve the performance & loading of the game.

## Gotchas

There's a few catches and exceptions we encountered trying to make this game work, so to save you a lot of hassle I went through them all and listed all the issues & their fixes accordingly :drill:

## Player

All player related functions can be found in the `player.cpp` file.
The HEV sound on death has been removed.,

### Damage

The player's damage is defined in the `player.cpp` file. At base, it is set to **100**.

## ConVars

In `GameDLL.cpp`, this *could* be where the literal game starts, but we just don't know.
When you call the `RegisterConVar` function from that file, you can successfully add a ConVar  into the game.

## Debug

When debug is set with the `DEBUG` macro, special game rules are applied within the game.
This can lead to a few gotchas, i.e:

- If the game is in `DEBUG` mode, the player will automatically be given a suit.

## Colours

Colors are set like this

```cpp
if (HL_RGB)
{
    (*r == 0) ? 124 : *r;
    (*g == 0) ? 84 : *g;
    (*b == 0) ? 12 : *b;
}
```
