---
title: Project Titan
description: |
  Project Titan is a story-driven game developed for the
  Macintosh II system.
date: 2022-09-07
tags: Azamaet
---

## Story

Set in the distant future, a war has broken out between the __Colonial Forces of Saturn__, and the revolutionary forces of the __Jupiter Revival Sect__ separatists.

In a last ditch attempt to sway the outcome of the war, the *CFS* takes it upon themselves to reopen the experimental Dyson Sphere on Helene, tearing a wormhole into the furthest reaches of deep space, in search of the largest source of light matter energy they could find, in a vain attempt to fuel their final weapon that could eliminate the *JRS* and win the war.

However, in doing so, the *CFS* unleashed a primordial force

After the destruction of earth during the final *Battle of Ganymede*, Hirana are left as the final unit of your squadron of the __Coalition of Federated Systems__ (CFS), leaving her stranded thousands of miles away from where the battlefield was.

Hirana's journey now leaves her dying, desperately clinging to respite by accruing the essence of the Void in order to sustain her injuries. Each character consumed by Void she takes on, she communicates with, discovering what the Void truly is, and how it aids her in helping locate her fleet commander.

With her fighter on its final legs, & oxygen supply almost fully depleted, Hirana is forced to let a part of the void spirit subsume her, just to keep her alive long enough to find her lover, and unit commander, who holds the keys to the *Vault of Souls* in the city of *[[raekkaeton]]* on Earth.

And as you make your way back through to the remains of earth, pieces of bodies and buildings are left scattered throughout space which only become more tightly grouped together and the space you traverse through becomes a beautiful abstract art piece as your mind tries to make sense of the horror you're in, with the void spirit slowly eating away at your soul, your ship starts to grow with the void infestation until it becomes an unrecognisable abomination, growing stronger as it defeats the newfound guardians of Saturn.

As it grows stronger your mind slips further as you're in constant war with the very thing keeping you alive until your sanity starts to slip completely, your ship broken and then finally - you find your lover and former commander sitting atop a pile of broken space debris, human organs & bones, with the newly formed shattered ring forming a halo above their heads.

## Entities

The world of Project Titan is home to a vast array of entities, sprawling from

![Void spirit entity](/static/content/art/voidspirit.jpg)

Written in [[hypertalk]]

