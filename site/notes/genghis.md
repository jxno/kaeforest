---
title: Genghis
description: Genghis is a multithreaded process watcher utility.
date: 2025-02-09
tags: dev
---

![Da terminalz](/static/content/img/Genghis.png)
###### v0.1 - smol. not even 1.0 release yet.

<https://gitlab.com/kaelta/genghis>

Genghis was written to be able to play, test and train multiple bots on [Lichess](https://lichess.org).
It contains a TUI to watch and monitor processes, and report back on resource usage.

Written in Rust of course.

## Compiling

```bash
nix develop
cargo b --release
```

## Usage

Put an engine into the `engines` directory, and in the `config.yml`, write a config based on one from the example. i.e:

```yaml
engines:
    - stockfish:
        name: "Stockfish"
        token: "lip_xxxxx"
        binary: "./engines/stockfish/stockfish"
```

The `binary` key is any command process you need to run your engine.
Whether it's Java needing a `java -jar ...` or whatever your chosen runtime is.

## Deployment

Todo.
