---
title: Archive
description: All tags on Kaeforest.
layout: layouts/home.njk
permalink: /tags/
tags: nav
order: 1
---

## All notes on Kaeforest.

<div style="display:flex;overflow-x:scroll;">
{% for tag in collections.tagList %}
  {% set tagUrl %}/tags/{{ tag | slug }}/{% endset %}
  <button>
    <a href={{tagUrl | url}}>[{{tag}}]</a>
  </button>
{% endfor %}
</div>

{% for entry in collections.notes | filterTagList | reverse  %}
{ {{entry.data.date | readableDate}} } - <a href="{{entry.url}}"> {{entry.data.title}} </a>
{% endfor %}
